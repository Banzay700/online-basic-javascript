let userName = prompt('Please, enter your name.');
let userAge = +prompt('Please, enter your age.');

const getUserData = () => {
  if (!userName) {
    userName = prompt('You did not enter your name. Please, enter your name again');
    getUserData();
  } else if (!userAge) {
    userAge = prompt('You did not enter your age. Please, enter your age again');
    getUserData();
  } else checkUserAge(userName, userAge);
};

const checkUserAge = (name, age) => {
  if (age > 22) alert(`Welcome ${name}`);
  if (age < 18) alert('You are not allowed to visit this website');
  if (age <= 22 && age >= 18) getUserAgree(name);
};

const getUserAgree = (name) => {
  const userAgree = confirm('Are you sure you want to continue?');

  if (userAgree) {
    alert(`Welcome ${name}`);
  } else {
    alert('You are not allowed to visit this website');
  }
};

getUserData();
