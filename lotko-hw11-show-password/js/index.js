const form = document.querySelector('.password-form'),
    submit = document.querySelector('button'),
    passIcon = document.querySelectorAll('i'),
    inputs = document.querySelectorAll('input[type="password"]')

passIcon.forEach(icon => {
    icon.addEventListener('click', e => {
        const input = e.target.previousSibling.previousSibling

        icon.classList.toggle('fa-eye-slash')
        input.type === 'text' ? (input.type = 'password') : (input.type = 'text')
    })
})

const validInput = () => {
    const alert = document.querySelector('.valid_password')
    const valid = /(?=^.{8,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/

    !valid.test(inputs[0].value) ? alert.classList.add('alert') : alert.classList.remove('alert')
    alert.classList.contains('alert') ? submit.setAttribute('disabled', '') : submit.removeAttribute('disabled', '')
}

const confirmPass = () => {
    const alert = document.querySelector('.conf_password')

    !inputs[0].value || inputs[0].value !== inputs[1].value ? alert.classList.add('alert') : alert.classList.remove('alert')
    alert.classList.contains('alert') ? submit.setAttribute('disabled', '') : submit.removeAttribute('disabled', '')
}

const showModal = () => {
    const title = document.querySelector('.form_title')

    form.classList.add('hidden')
    title.textContent = 'Welcome!'
}

inputs[0].addEventListener('blur', validInput)
inputs[1].addEventListener('blur', confirmPass)

submit.addEventListener('click', () => {
    validInput()
    confirmPass()
    submit.hasAttribute('disabled') ? '' : showModal()
})

form.addEventListener('submit', e => {
    e.preventDefault()
})
