const someArray = ['hello', '2312', 2312, , null, 100]
const filterBy = (info, dataType) => info.filter(value => typeof value !== dataType)

console.log(filterBy(someArray, 'string'))
