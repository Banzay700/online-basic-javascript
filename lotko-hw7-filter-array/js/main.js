const someArray = ['hello', '2312', 2312, , null, 100, {}]

const filterBy = (info, dataType) => {
    const result = info.filter(value => {
        return typeof value !== dataType
    })

    return result
}

console.log(filterBy(someArray, 'string'))
