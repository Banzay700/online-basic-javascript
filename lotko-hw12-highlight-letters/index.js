const letters = document.querySelectorAll('.btn')

document.addEventListener('keydown', e => {
    letters.forEach(letter => {
        letter.classList.remove('active')

        letter.textContent == e.key ? letter.classList.add('active') : ''
    })
})
