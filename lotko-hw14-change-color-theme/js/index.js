const page = document.documentElement,
   themeToggle = document.querySelector('.toggle'),
   toggleSwitch = themeToggle.firstElementChild

localStorage.theme ? (page.dataset.theme = localStorage.getItem('theme')) : ''

page.dataset.theme === 'dark' ? toggleSwitch.classList.add('sun') : ''

themeToggle.addEventListener('click', () => {
   toggleSwitch.classList.toggle('sun')
   page.dataset.theme === 'dark' ? (page.dataset.theme = '') : (page.dataset.theme = 'dark')
   localStorage.theme === 'dark' ? (localStorage.theme = '') : localStorage.setItem('theme', 'dark')
})
