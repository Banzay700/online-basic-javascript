const paragraph = document.querySelectorAll('p'),
    optionsList = document.getElementById('optionsList'),
    mainHeaderElements = document.querySelectorAll('.main-nav-item'),
    title = document.querySelectorAll('.section-title')

// change backgroundColor & add paragraph 'This is a paragraph' to element
paragraph.forEach(item => {
    item.style.backgroundColor = '#ff0000'
    item.id === 'testParagraph' ? (item.textContent = 'This is a paragraph') : ''
})

// find parent & children nodes of optionsList
console.log(optionsList)
console.log(optionsList.parentNode)

optionsList.childNodes.forEach(item => {
    console.log(item.nodeName)
    console.log(item.nodeType)
})

// add className 'nav-item'
console.log(mainHeaderElements)
mainHeaderElements.forEach(item => item.classList.add('nav-item'))

// remove className 'section-title'
title.forEach(item => item.classList.remove('section-title'))
