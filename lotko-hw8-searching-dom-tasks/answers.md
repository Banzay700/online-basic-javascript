# HW 8 Searching DOM Tasks

### _Питання_

1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

### _Відповіді_

1. DOM - об'єктна модель документа, яку створює браузер, для того щоб було легше маніпулювати елементами документа (шукати, отримувати і додавати елементи документа)

2. Різниця між властивостями HTML-елементів innerHTML та innerText в тому, що innerHTML може отримувати та встановлювати вміст в форматі HTML, a innerText додає лише текст.

3. До елемента сторінки можна звернутися за допомогою:

```javascript
document.createElement
document.querySelector
document.querySelectorAll
document.getElementById
document.getElementsByName
document.getElementsByTagName
document.getElementsByClassName
```

Усі методи можна використовавати в залежності від завдання, яке стоїть перед розробником. Тому "кращий" виділити важко.
