const userFirstName = document.querySelector('#firstName')
const userLastName = document.querySelector('#lastName')
const userDateOfBirth = document.querySelector('#birthday')
const button = document.querySelector('#button')
const userLogin = document.querySelector('#login')
const userPassword = document.querySelector('#password')
const userAge = document.querySelector('#age')

const createNewUser = (first, last, db) => {
    const newUser = {
        firstName: first,
        lastName: last,
        birthday: db,

        getLogin() {
            return this.firstName.trim()[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getPassword() {
            const year = this.birthday.slice(6, 10)
            return this.firstName.trim()[0].toUpperCase() + this.lastName.toLowerCase() + year
        },
        getAge() {
            const convertBirthday = () => {
                const [day, month, year] = this.birthday.split('.')
                return new Date([month, day, year].join('.'))
            }
            const birthday = convertBirthday()
            return new Date(Date.now() - birthday).getFullYear() - new Date(0).getFullYear()
        },
    }

    return newUser
}

const showUserInfo = () => {
    const user = createNewUser(userFirstName.value, userLastName.value, userDateOfBirth.value)
    userLogin.innerText = user.getLogin()
    userPassword.innerText = user.getPassword()
    userAge.innerText = user.getAge()
}

button.addEventListener('click', showUserInfo)
