const tabsTitle = document.querySelectorAll('.tabs-title'),
   tabsContent = document.querySelectorAll('.tab-description')

tabsTitle[0].classList.add('tab-active')
tabsContent[0].classList.add('info-active')

const addAttribute = (array, attributName) => {
   array.forEach((item, index) => {
      item.setAttribute(`${attributName}`, `${index}`)
   })
}

const showTabDescription = () => {
   tabsTitle.forEach(tab => {
      tab.addEventListener('click', () => {
         const tabNumber = tab.dataset.tab
         const tabDescription = document.querySelector(`*[data-tab-info="${tabNumber}"]`)

         tabsTitle.forEach(tab => tab.classList.remove('tab-active'))
         tabsContent.forEach(desc => desc.classList.remove('info-active'))

         tab.classList.add('tab-active')
         tabDescription.classList.add('info-active')
      })
   })
}

addAttribute(tabsTitle, 'data-tab')
addAttribute(tabsContent, 'data-tab-info')
showTabDescription()
