let index = 0,
   seconds = 2,
   milliseconds = 99,
   startCircle = true;

const sec = document.querySelector('.sec'),
   mlSec = document.querySelector('.mlsec'),
   button = document.querySelector('.btn'),
   slides = document.querySelectorAll('.image-to-show');

function startTimer() {
   setInterval(() => {
      if (startCircle) {
         seconds === -1 ? slides[index++ % slides.length].classList.add('hidden') : '';
         seconds === -1 ? (seconds = 2) : '';
         slides[index % slides.length].classList.remove('hidden');

         sec.textContent = '0' + seconds;
         mlSec.textContent = milliseconds;

         milliseconds--;
         if (milliseconds === 0) {
            seconds--;
            milliseconds = 99;
         }
      }
   }, 10);
}
startTimer();

button.addEventListener('click', () => {
   startCircle = !startCircle;

   button.classList.toggle('btn-start');
   button.textContent == 'STOP' ? (button.textContent = 'START') : (button.textContent = 'STOP');
});
