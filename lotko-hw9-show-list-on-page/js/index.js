const cities = ['Kharkiv', 'Kyiv', 'Odessa', 'Lviv', 'Dnipro', 'Simferopol']

const createList = (arr, parent = document.body) => {
    const ul = document.createElement('ul')
    parent.prepend(ul)
    arr.forEach(item => ul.insertAdjacentHTML('beforeend', `<li>${item}</li>`))
}

createList(cities)
