# HW9 Show list on page

### _Питання_

1. Опишіть, як можна створити новий HTML тег на сторінці.
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
3. Як можна видалити елемент зі сторінки?

### _Відповіді_

1. Hовий HTML тег можна створити за допомогою:

```JavaScript
document.createElement("div")
element.insertAdjacentHTML('afterbegin', '<div>123123123</div>')
element.innerHTML = '<div></div>'

```

2. Означає місце куди буде вставлений новий елемент відносно бітьківського елемента.

```JavaScript
'afterbegin' // на початку батьківського елемента
'beforeend' // в кінці батьківського елемента
'beforebegin' // перед батьківським елементом
'afterend' // після батьківського елемента
```

3. Видалити елемент можна за допомогою `remove()` , `removeChild()` або присвоїти пустий рядок `element.innerHTML = ''`
