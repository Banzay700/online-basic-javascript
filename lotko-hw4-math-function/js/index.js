getUserData = () => {
    let firstNum = +prompt('Please, enter FIRST number');
    let secondNum = +prompt('Please, enter SECOND number');
    let operator = prompt('Please, enter OPERATOR');

    showResults(firstNum, secondNum, operator);
};

const showResults = (num1, num2, symbol) => {
    if (symbol === '*') console.log(`${num1} * ${num2} =`, num1 * num2);
    if (symbol === '/') console.log(`${num1} / ${num2} =`, num1 / num2);
    if (symbol === '+') console.log(`${num1} + ${num2} =`, num1 + num2);
    if (symbol === '-') console.log(`${num1} - ${num2} =`, num1 - num2);
};

getUserData();
